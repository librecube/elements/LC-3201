# LibreEquipment MGSE 1U TestPOD

The 1U TestPOD is used for vibration testing of 1U CubeSats.

![](docs/pictures/main.jpg)

## Getting Started

Find the design source files in folder `/src`. All information and files needed
for manufacturing are in folder `/build`.
